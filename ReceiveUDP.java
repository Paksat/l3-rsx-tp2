

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class ReceiveUDP {
	public static void main(String[] args) throws Exception{
		// je recuper ici le numero de port donn�e par l'utilisateur
		int port = Integer.parseInt(args[0]);

		// J'ouvre ma socket sur un port en �coute
		DatagramSocket server = new DatagramSocket(port);

		while(true){
			byte[] buffer = new byte[8192];
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
			server.receive(packet);
					
			server.send(packet);
					
			String str = new String(packet.getData());
			print("Recu de la part de " + packet.getAddress() 
			+ " sur le port " + packet.getPort() + " : ");
			println(str);

			packet.setLength(buffer.length);				
		}
	}   
	
	   public static synchronized void print(String str){
		      System.out.print(str);
		   }
		   public static synchronized void println(String str){
		      System.err.println(str);
		   }

}
