
import java.io.*;
import java.net.*;

public class SendUDP {
	final static int taille = 2048;
	final static byte buffer[] = new byte[taille];

	public static void main(String[] args) throws Exception{
		
		String addr = args[0];
		int port = Integer.parseInt(args[1]);
		String msg = args[2];
	
		int taille = msg.length();

		InetAddress serveur = InetAddress.getByName(addr);

		SocketAddress sockAddr = new InetSocketAddress(serveur, port);

		
		byte buffer[] = msg.getBytes();
		DatagramPacket donneEnv = new DatagramPacket(buffer,taille,serveur,port);
	
		DatagramSocket socket = new DatagramSocket();


		socket.send(donneEnv);

		DatagramPacket donneRec = new DatagramPacket(new byte[taille], taille);
		socket.receive(donneRec);
		System.out.println("Donne recu par :" + new String(donneRec.getData()));
		System.out.println("Depuis  : " + donneRec.getAddress() + " sur le port " + donneRec.getPort() );
	}
}
